-- Create Sequence t_notebook_transaction_history_seq
CREATE SEQUENCE notebook_shop.t_notebook_transaction_history_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE
	START 1
	NO CYCLE;

-- Create table t_notebook_transaction_history
CREATE TABLE notebook_shop.t_notebook_transaction_history (
	id BIGINT NOT NULL DEFAULT NEXTVAL('notebook_shop.t_notebook_transaction_history_seq'::regclass),
	created_date TIMESTAMP NOT NULL,
	created_by BIGINT NOT NULL,
	modified_date TIMESTAMP,
	modified_by BIGINT,
	product_name VARCHAR(100),
	product_category VARCHAR(100),
	product_brand VARCHAR(100),
	product_type VARCHAR(100),
	quantity_sold NUMBER,
	unit_price NUMBER,
	unit_detail TEXT,
	additional_data_1 TEXT,
	additional_data_2 TEXT,
	additional_data_3 TEXT,
	additional_data_4 TEXT,
	additional_data_5 TEXT,
	CONSTRAINT t_notebook_transaction_history PRIMARY KEY(id)
);

-- Create index for t_notebook_transaction_history
CREATE INDEX t_notebook_transaction_history_created_by_idx ON notebook_shop.t_notebook_transaction_history(created_by);
