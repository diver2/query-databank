-- Create Sequence m_user_seq
CREATE SEQUENCE user_mgt.m_user_seq
	INCREMENT BY 1
	MINVALUE 100
	NO MAXVALUE
	START 100
	NO CYCLE;

-- Create table m_user
CREATE TABLE user_mgt.m_user (
	id BIGINT NOT NULL DEFAULT NEXTVAL('user_mgt.m_user_seq'::regclass),
	created_date TIMESTAMP NOT NULL,
	created_by BIGINT NOT NULL,
	modified_date TIMESTAMP,
	modified_by BIGINT,
	nick_name VARCHAR(50),
	full_name VARCHAR(100),
	phone_number VARCHAR(20),
	email VARCHAR (50),
	status INT,
	superior_id BIGINT,
	CONSTRAINT m_user_pk PRIMARY KEY(id)
);

-- Create index for m_user
CREATE INDEX m_user_superior_id_idx ON user_mgt.m_user USING HASH (superior_id);
