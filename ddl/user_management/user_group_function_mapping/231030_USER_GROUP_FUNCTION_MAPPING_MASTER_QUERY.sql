-- Create table m_user_group_function_mapping
CREATE TABLE user_mgt.m_user_group_function_mapping (
	user_group_id BIGINT NOT NULL,
	function_id BIGINT NOT NULL,
	CONSTRAINT m_user_group_function_mapping_pk PRIMARY KEY(user_group_id, function_id)
);
