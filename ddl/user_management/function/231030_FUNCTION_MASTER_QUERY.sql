-- Create Sequence m_function_seq
CREATE SEQUENCE user_mgt.m_function_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE
	START 1;

-- Create table m_function
CREATE TABLE user_mgt.m_function (
	id BIGINT NOT NULL DEFAULT NEXTVAL('user_mgt.m_function_seq'::regclass),
	created_date TIMESTAMP NOT NULL,
	created_by BIGINT NOT NULL,
	modified_date TIMESTAMP,
	modified_by BIGINT,
	name VARCHAR(50) UNIQUE,
	description VARCHAR(255),
	value VARCHAR(255),
	CONSTRAINT m_function_pk PRIMARY KEY(id)
);
