-- Create Sequence m_user_auth_seq
CREATE SEQUENCE user_mgt.m_user_auth_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE
	START 1
	NO CYCLE;
	
-- Create table m_user_auth
CREATE TABLE user_mgt.m_user_auth (
	id BIGINT NOT NULL DEFAULT NEXTVAL('user_mgt.m_user_auth_seq'::regclass),
	created_date TIMESTAMP NOT NULL,
	created_by BIGINT NOT NULL,
	modified_date TIMESTAMP,
	modified_by BIGINT,
	user_name VARCHAR(50) NOT NULL UNIQUE,
	password TEXT NOT NULL,
	is_deleted BOOLEAN DEFAULT FALSE,
	user_id BIGINT NOT NULL,
	CONSTRAINT m_user_auth_pk PRIMARY KEY(id)
);

-- Create index for m_user_auth
CREATE INDEX m_user_auth_user_id_idx ON user_mgt.m_user_auth USING HASH (user_id);
CREATE INDEX m_user_auth_user_name_idx ON user_mgt.m_user_auth USING HASH (user_name);
