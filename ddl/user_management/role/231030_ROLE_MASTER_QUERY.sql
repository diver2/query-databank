-- Create Sequence m_role_seq
CREATE SEQUENCE user_mgt.m_role_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE
	START 1
	NO CYCLE;
	
-- Create table m_role
CREATE TABLE user_mgt.m_role (
	id BIGINT NOT NULL DEFAULT NEXTVAL('user_mgt.m_role_seq'::regclass),
	created_date TIMESTAMP NOT NULL,
	created_by BIGINT NOT NULL,
	modified_date TIMESTAMP,
	modified_by BIGINT,
	name VARCHAR(50) UNIQUE,
	description VARCHAR(255),
	CONSTRAINT m_role_pk PRIMARY KEY(id)
);
