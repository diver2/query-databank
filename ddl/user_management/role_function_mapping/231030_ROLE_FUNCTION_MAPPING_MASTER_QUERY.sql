-- Create table m_role_function_mapping
CREATE TABLE user_mgt.m_role_function_mapping (
	role_id BIGINT NOT NULL,
	function_id BIGINT NOT NULL,
	CONSTRAINT m_role_function_mapping_pk PRIMARY KEY(role_id, function_id)
);
