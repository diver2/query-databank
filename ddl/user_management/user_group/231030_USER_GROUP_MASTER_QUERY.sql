-- Create Sequence m_user_group_seq
CREATE SEQUENCE user_mgt.m_user_group_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE
	START 1
	NO CYCLE;
	
-- Create table m_user_group
CREATE TABLE user_mgt.m_user_group (
	id BIGINT NOT NULL DEFAULT NEXTVAL('user_mgt.m_user_group_seq'::regclass),
	created_date TIMESTAMP NOT NULL,
	created_by BIGINT NOT NULL,
	modified_date TIMESTAMP,
	modified_by BIGINT,
	name VARCHAR(50) UNIQUE,
	description VARCHAR(255),
	CONSTRAINT m_user_group_pk PRIMARY KEY(id)
);
