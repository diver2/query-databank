-- Create table m_user_group_mapping
CREATE TABLE user_mgt.m_user_group_mapping (
	user_id BIGINT NOT NULL,
	user_group_id BIGINT NOT NULL,
	CONSTRAINT m_user_group_mapping_pk PRIMARY KEY(user_id, user_group_id)
);
