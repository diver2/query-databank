-- Create table m_user_role_mapping
CREATE TABLE user_mgt.m_user_role_mapping (
	user_id BIGINT NOT NULL,
	role_id BIGINT NOT NULL,
	CONSTRAINT m_user_role_mapping_pk PRIMARY KEY(user_id, role_id)
);
