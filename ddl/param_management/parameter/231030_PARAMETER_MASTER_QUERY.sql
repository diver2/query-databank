-- Create Sequence m_parameter_seq
CREATE SEQUENCE param_mgt.m_parameter_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE
	START 1
	NO CYCLE;

-- Create table m_parameter
CREATE TABLE param_mgt.m_parameter (
	id BIGINT NOT NULL DEFAULT NEXTVAL('param_mgt.m_parameter_seq'::regclass),
	created_date TIMESTAMP NOT NULL,
	created_by BIGINT NOT NULL,
	modified_date TIMESTAMP,
	modified_by BIGINT,
	module VARCHAR(50),
	key VARCHAR(50),
	value1 VARCHAR(255),
	value2 VARCHAR(255),
	value3 VARCHAR(255),
	value4 VARCHAR(255),
	value5 VARCHAR(255),
	value6 TEXT,
	value7 TEXT,
	is_encrypted BOOLEAN DEFAULT FALSE,
	is_deleted BOOLEAN DEFAULT FALSE,
	order INT,
	CONSTRAINT m_parameter_pk PRIMARY KEY(id)
);

-- Create index for m_user
CREATE INDEX m_parameter_module_idx ON param_mgt.m_parameter(module);
CREATE INDEX m_parameter_key_idx ON param_mgt.m_parameter("key");
CREATE INDEX m_parameter_order_idx ON param_mgt.m_parameter("order");